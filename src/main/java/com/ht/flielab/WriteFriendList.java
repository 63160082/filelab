/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.flielab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class WriteFriendList {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        
        try {
            ArrayList <Friend> friendlist = new ArrayList<Friend>();
            friendlist.add(new Friend("Hathai" , 20 , "0801800244"));
            friendlist.add(new Friend("Belle" , 20 , "0811811245"));            
            friendlist.add(new Friend("Faye" , 18 , "0851822246"));     
            
            //Friend friend1 = new Friend("Hathai" , 20 , "0801800244");
            //Friend friend2 = new Friend("Belle" , 20 , "0811811245");
            //Friend friend3 = new Friend("Faye" , 18 , "0851822246");
            
            File file = new File("list_of_friend.dat"); //create file
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendlist);
            
            oos.close();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
