/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.flielab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class ReadPlayer {
    public static void main(String[] args) {
        Player O = null;
        Player X = null;
        
        FileInputStream fis = null;
        
        try {
            File file = new File("Players.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            O = (Player) ois.readObject();
            X = (Player) ois.readObject();
            
            ois.close();
            fis.close();
            
        } catch (FileNotFoundException ex) {
            X = new Player('X');
            O = new Player('O');
        } catch (IOException ex) {
            Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(fis!=null) {
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            System.out.println(X);
            System.out.println(O);        
        
    }
}
