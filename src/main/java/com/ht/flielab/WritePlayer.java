/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.flielab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class WritePlayer  {
    public static void main(String[] args) {
        Player O = new Player('O');
        Player X = new Player('X');
        
        X.win();
        O.loss();
        X.draw();
        O.draw();
        X.win();
        O.loss();
        System.out.println(X);
        System.out.println(O);
        
        FileOutputStream fos = null;
        
        try {
            File file = new File("Players.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            oos.writeObject(X);
            oos.writeObject(O);
            
            oos.close();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
